var url = require('url');
var http = require('http');
var xml_lib = require('libxmljs-easy');
var fs = require('fs');

var url_options = url.parse('http://static.espncricinfo.com/rss/livescores.xml');

var response_data = '';

var http_req = http.get(url_options, function(response) {
    console.log("Got response: " + response.statusCode );
    response.on('data', function(chunk) {
	response_data += chunk;
    });
    response.on('end', function() {
	var xml = xml_lib.parse(response_data);
	console.log(xml.channel[0].item[0].title[0].$.text());
	var matches = Array.prototype.slice.call(xml.channel[0].item);
	var urls = matches.map(function(match) {
	    return { name: (match.title[0].$.text()), 
		     url: (match.guid[0].$.text())};
	});
	fs.writeFileSync('live-matches.json', JSON.stringify(urls, null, '  '));
    });
});

